package loader

import (
	"bufio"
	"os"
)

func loadFile(file string) (urls []string, err error) {
	fh, err := os.Open(file)
	if nil != err {
		return
	}
	defer fh.Close()

	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		urls = append(urls, scanner.Text())
	}
	return
}
