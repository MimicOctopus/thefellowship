package loader

import (
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	config "github.com/MimicOctopus/goutils/config"
)

// Config described the loader configuration
type Config struct {
	BanQ    string
	InQ     string
	FailQ   string
	MsgSize int
}

// NewFromFile inits the bare Parser from the provided config file
func NewFromFile(file string, queueMng q.QueueManager) (l *Loader, err error) {
	conf := Config{}
	err = config.ReadConfFile(&conf, file)
	l = NewLoader(queueMng, conf)
	return
}

// CreateConfFile creates a new, empty config file with the given location
func CreateConfFile(file string) (err error) {
	conf := Config{"ban-urls", "raw-urls", "fail-urls", 100}
	return config.WriteConfFile(&conf, file)
}
