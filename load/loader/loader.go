package loader

import (
	"errors"
	m "gitlab.com/MimicOctopus/thefellowship/messages"
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	t "gitlab.com/MimicOctopus/thefellowship/types"
	"time"
)

// Loader is a struct enabling loading of urls
type Loader struct {
	queueMng q.QueueManager
	conf     Config
	sender   chan interface{}
	done     chan bool
}

// NewLoader creates a new Loader struct with the specified parameters
func NewLoader(queueMng q.QueueManager, conf Config) *Loader {
	return &Loader{queueMng, conf, make(chan interface{}, 1), make(chan bool, 1)}
}

// Load the urls from the specified file
func (l Loader) Load(file string) (err error) {
	urls, err := loadFile(file)
	if nil != err {
		return
	}

	go l.queueMng.Send(l.conf.InQ, m.PackDomains, l.sender, l.done)
	send := t.UrlsToDomainResourcesArray(urls)
	l.sender <- send

	return l.Stop()
}

// Ban the domains from the specified file
func (l Loader) Ban(file string) (err error) {
	domains, err := loadFile(file)
	if nil != err {
		return
	}

	go l.queueMng.Send(l.conf.BanQ, m.PackBans, l.sender, l.done)
	l.sender <- domains

	return l.Stop()
}

// Stop the loader
func (l Loader) Stop() error {
	time.Sleep(time.Second * 3) // Let some time for the dust to fall!
	close(l.sender)
	d := <-l.done
	if !d {
		return errors.New("crapped the hell out")
	}
	return nil
}
