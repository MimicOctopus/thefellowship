package main

import (
	"fmt"
	"gitlab.com/MimicOctopus/thefellowship/load/loader"
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	"log"
	"os"
	"runtime"
)

const (
	load      = "load"
	ban       = "ban"
	initt     = "init"
	usage     = `Usage: loader {init|load <file>|ban <file>}`
	confDir   = "config"
	queueFile = confDir + "/queue.conf"
	confFile  = confDir + "/loader.conf"
)

func main() {

	runtime.GOMAXPROCS(2)
	if len(os.Args) >= 3 {
		s := new(q.StompManager)
		err := s.InitFromFile(queueFile)
		if nil != err {
			log.Print("Couldn't connect to stomp!")
			log.Fatal(err)
		}
		l, err := loader.NewFromFile(confFile, s)
		if nil != err {
			log.Print("Could not initialize loader!")
			log.Fatal(err)
		}
		switch os.Args[1] {
		case load:
			err = l.Load(os.Args[2])
		case ban:
			err = l.Ban(os.Args[2])
		default:
			fmt.Println(usage)
		}
		if nil != err {
			log.Print("Operation failed!")
		}
		return
	} else if len(os.Args) > 1 {
		switch os.Args[1] {
		case initt:
			s := new(q.StompManager)
			err := s.CreateConfFile(queueFile)
			if nil != err {
				log.Print("Can't create stomp conf")
				log.Print(err)
			}
			err = loader.CreateConfFile(confFile)
			if nil != err {
				log.Print("Can't create loader config file")
				log.Fatal(err)
			}
		default:
			fmt.Println(usage)
		}
		return
	}
	fmt.Println(usage)
}
