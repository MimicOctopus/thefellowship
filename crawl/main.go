package main

import (
	"fmt"
	"log"
	"os"
	"runtime"

	"gitlab.com/MimicOctopus/thefellowship/crawl/crawler"
	"gitlab.com/MimicOctopus/thefellowship/crawl/queuer"
)

const (
	server    = "server"
	initt     = "init"
	usage     = `Usage: crawler {init|server}`
	queueFile = "config/queue.conf"
)

func main() {
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case server: // Launch server
			runtime.GOMAXPROCS(1)
			s := new(queuer.StompManager)
			err := s.ConnectFromFile(queueFile)
			if nil != err {
				log.Print("Couldn't connect to stomp!")
				log.Fatal(err)
			}
			s.Run()
			c := crawler.NewCrawler(s)
			c.Crawl(50)
		case initt:
			s := new(queuer.StompManager)
			s.CreateConfFile(queueFile)
		default:
			fmt.Println(usage)
		}
	} else {
		fmt.Println(usage)
	}
}
