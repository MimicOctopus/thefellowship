package crawler

import (
	"log"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitlab.com/MimicOctopus/thefellowship/crawl/queuer"
	"github.com/microcosm-cc/bluemonday"
)

var timeout = time.Duration(10 * time.Second)

type Crawler struct {
	queueMng queuer.QueueManager
	client   http.Client
	p        *bluemonday.Policy
}

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

func NewCrawler(queueMng queuer.QueueManager) *Crawler {
	transport := http.Transport{
		Dial: dialTimeout,
	}

	client := http.Client{
		Transport: &transport,
	}
	return &Crawler{queueMng, client, bluemonday.UGCPolicy()}
}

func (c Crawler) Crawl(nbWorker int) {
	var wg sync.WaitGroup
	wg.Add(nbWorker)
	for i := 0; i < nbWorker; i++ {
		go c.receiveAndSend()
	}
	wg.Wait()
}

func (c Crawler) obtainHeader(url string) (http.Header, error) {
	header, err := c.client.Head(url)
	if nil != err {
		return nil, err
	}
	header.Body.Close()
	return header.Header, nil
}

func (c Crawler) getContent(url string) ([]byte, error) {
	response, err := c.client.Get(url)
	if nil != err {
		return nil, err
	}
	defer response.Body.Close()
	//contents, err := ioutil.ReadAll(response.Body)
	contents := c.p.SanitizeReader(response.Body).Bytes()
	return contents, err
}

func (c Crawler) receiveAndSend() {
	receiver := c.queueMng.Receive()
	sender := c.queueMng.Sender()
	fail := c.queueMng.Fail()
	for {
		urls := <-receiver
		log.Print("Managing URLs: ", len(receiver))
		for _, url := range urls {
			log.Print("Working on: ", url)
			header, err := c.obtainHeader(url)
			if nil != err {
				log.Print(err)
				fail <- url
				continue
			}
			contentType := header.Get("content-type")
			if !strings.Contains(contentType, "text/html") {
				log.Print("Content not html!")
				continue
			}
			contents, err := c.getContent(url)
			if nil != err { // Eventually tell hURLer
				log.Print(err)
				fail <- url
				continue
			}
			sender <- contents
		}
	}
}
