package queuer

type QueueManager interface {
	Sender() chan<- []byte    // A channel to send the retrieved html
	Receive() <-chan []string // A channel to receive the URL requests
	Fail() chan<- string      // A channel to notify of failures
}
