package queuer

import (
	"encoding/json"
	m "gitlab.com/MimicOctopus/thefellowship/messages"
	"gitlab.com/MimicOctopus/thefellowship/types"
	c "github.com/MimicOctopus/goutils/config"
	stomp "github.com/guotie/stomp"
	"log"
	"os"
	"time"
)

type StompManager struct {
	conf      stompManagerConf
	conn      *stomp.Conn
	outConn   *stomp.Conn
	inSub     *stomp.Subscription
	outTopic  string
	failQueue string
	in        chan []string
	out       chan []byte
	fail      chan string
}

type stompManagerConf struct {
	Proto     string
	Addr      string
	Opts      stomp.Options
	OutTopic  string
	InQueue   string
	FailQueue string
}

func (s *StompManager) ConnectStomp(proto string, addr string, opts stomp.Options) error {
	conn, err := stomp.Dial("tcp", addr, opts)
	if nil != err {
		return err
	}
	s.conn = conn
	outConn, err := stomp.Dial("tcp", addr, opts)
	if nil != err {
		return err
	}
	s.outConn = outConn
	return nil
}

func (s *StompManager) ConnectFromFile(file string) error {
	fh, err := os.Open(file)
	if nil != err {
		return err
	}

	dec := json.NewDecoder(fh)
	var conf stompManagerConf
	err = dec.Decode(&conf)
	if nil != err {
		return err
	}

	s.conf = conf
	err = s.Connect()
	return err
}

func (s *StompManager) Connect() error {
	err := s.ConnectStomp(s.conf.Proto, s.conf.Addr, s.conf.Opts)
	if nil != err {
		return err
	}
	err = s.SubscribeIncoming(s.conf.InQueue)
	if nil != err {
		return err
	}
	s.SetOutTopic(s.conf.OutTopic)
	s.SetFailQueue(s.conf.FailQueue)
	return err
}

func (s *StompManager) CreateConfFile(file string) error {
	dbconf := stompManagerConf{"tcp", "localhost", stomp.Options{}, "out", "in", "fails"}
	err := c.WriteConfFile(&dbconf, file)
	return err
}

func (s *StompManager) SubscribeIncoming(queueName string) error {
	var err error
	s.inSub, err = s.conn.Subscribe(queueName, stomp.AckAuto)
	if nil == s.in {
		s.in = make(chan []string, 10)
	}
	return err
}

func (s *StompManager) SetOutTopic(outTopic string) {
	s.outTopic = outTopic
	if s.out == nil {
		s.out = make(chan []byte, 10)
	}
}

func (s *StompManager) SetFailQueue(failQueue string) {
	s.failQueue = failQueue
	if s.fail == nil {
		s.fail = make(chan string, 10)
	}
}

func (s *StompManager) Sender() chan<- []byte {
	return s.out
}

func (s *StompManager) Fail() chan<- string {
	return s.fail
}

func (s *StompManager) Receive() <-chan []string {
	return s.in
}

func (s *StompManager) sender() {
	max := 1
	ticker := time.NewTicker(time.Millisecond * 2000)
	fails := make([]string, max) //Constantly reusing this buffer
	fcount := 0
	for {
		select {
		case out := <-s.out:
			data, err := m.CompressHTML(string(out))
			if nil != err {
				log.Print("Error marshalling HTML: ", err)
				continue
			}
			err = s.outConn.Send(s.outTopic, "", data, nil)
			if nil != err {
				log.Print(err)
				s.Connect()
				s.out <- out
				continue
			}
		case fail := <-s.fail:
			fails[fcount] = fail
			fcount = (fcount + 1) % max
			if fcount != 0 {
				continue
			}
			log.Print("Sending Fails...")
			data, err := m.CompressURLs(fails)
			if nil != err {
				log.Print("Error mashaling fail urls: ", err)
				continue
			}
			err = s.outConn.Send(s.failQueue, "", data, nil)
			if nil != err {
				log.Print(err)
				s.Connect()
				continue
			}
			log.Print("Fails Sent")
		case <-ticker.C:
			log.Print("Sender timeout")
		}
	}
}

func (s *StompManager) receiver() {
	ticker := time.NewTicker(time.Millisecond * 1000)
	for {
		select {
		case msg := <-s.inSub.C:
			log.Print("Dequeued URLs")
			if nil != msg.Err {
				log.Print(msg.Err)
				s.Connect()
				continue
			}
			urls, err := m.UncompressURLs(msg.Body)
			if nil != err {
				log.Print("Invalid bundle!: ", err)
				continue
			}
			s.in <- urls
		case <-ticker.C: // Timeout, Carry on
			log.Print("receiver timeout")
			continue
		}
	}
}

func (s *StompManager) Run() {
	go s.sender()
	go s.receiver()
}
