# The followship

Distributed web-crawling experiment.

# Messaging

Messaging will rely on Stomp. Using gzip and protobuf for efficiency and
simplicity.

# hURLer (pending better name)

The idea is to ingest URLs from other systems and then spit out previously
unseen URL for processing. Additionally we make a best attempt at respecting the
content of the robots.txt and may decide not to visit a site if it's not
present. As we tweak the functionality, this behavior may change.

## Backend

Current implementation is based on Postgresql. It may work with other SQL
platforms or require adaptation. The Postgres implementation also makes heavy
use of SQL functions and utilities.

## Cross-Compile

`env GOOS=freebsd GOARCH=amd64 go build .` in the proper project directory
