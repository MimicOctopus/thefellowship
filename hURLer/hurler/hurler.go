package hurler

import (
	"log"
	"sync"

	"gitlab.com/MimicOctopus/thefellowship/hURLer/persist"
	"gitlab.com/MimicOctopus/thefellowship/hURLer/queuer"
	"gitlab.com/MimicOctopus/thefellowship/types"
)

// Hurler is a hURLer implementation
type Hurler struct {
	back     persist.HURLerPersistor
	queueMng queuer.QueueManager
	msgSize  int
}

// NewHurler creates a new Hurler struct with the specified parameters
func NewHurler(back persist.HURLerPersistor, queueMng queuer.QueueManager, msgSize int) *Hurler {
	return &Hurler{back, queueMng, msgSize}
}

// Work creates the specified number of workers and runs them
func (h Hurler) Work(nbWorker int) {
	var wg sync.WaitGroup
	wg.Add(nbWorker)
	for i := 0; i < nbWorker; i++ {
		go h.receiveAndSend()
	}
	wg.Wait()
}

// receiveAndSend (name not appropriate exactly) anymore manages the core routine for each worker
func (h Hurler) receiveAndSend() {
	var send []types.DomainResources
	var count int
	receiver := h.queueMng.Receive()
	bansrec := h.queueMng.Bans()
	failrec := h.queueMng.Fail()
	sender := h.queueMng.GetSend()
	send = make([]types.DomainResources, h.msgSize)
	for {
		select {
		case urls := <-receiver:
			//log.Print("Managing URLs: ", len(receiver))
			for _, domRes := range urls {
				if domRes == nil {
					continue
				}
				pRessources, err := h.back.ManageIncomingDomainRessources(domRes)
				if nil != err {
					log.Print(err)
					continue
				}
				if nil == pRessources || len(pRessources.ProtoRes()) == 0 {
					continue
				}

				send[count] = pRessources
				count++

				if count == h.msgSize {
					sender <- send
					send = make([]types.DomainResources, h.msgSize)
					count = 0
				}
			}
		case bans := <-bansrec:
			for _, sban := range bans {
				ban, err := h.back.GetOrInsertDomain(sban)
				if nil != err {
					log.Print(err)
					continue
				}
				err = h.back.BanDomain(ban.GetID())
			}
		case fails := <-failrec:
			for _, domRes := range fails {
				d, err := h.back.GetOrInsertDomain(domRes.Domain())
				if nil != err {
					log.Print(err)
					continue
				}
				for proto, resources := range domRes.ProtoRes() {
					for _, resource := range resources {
						id, err := h.back.GetRessourceID(d.GetID(), proto, resource)
						if nil != err {
							log.Print("Cannot retrieve URL ID!: ", err)
							log.Print(resource)
							continue
						}
						err = h.back.MarkRessourceFail(id)
						if nil != err {
							log.Print("Cannot mark URL Failed: ", err)
						}
					}
				}
			}
		}
	}
}
