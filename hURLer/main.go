package main

import (
	"fmt"
	"gitlab.com/MimicOctopus/thefellowship/hURLer/hurler"
	per "gitlab.com/MimicOctopus/thefellowship/hURLer/persist"
	"gitlab.com/MimicOctopus/thefellowship/hURLer/queuer"
	"log"
	"os"
	"runtime"
	//"net/http"
	//_ "net/http/pprof"
)

const (
	server    = "server"
	initt     = "init"
	dbInit    = "dbinit"
	usage     = `Usage: hurler {init|dbinit|server}`
	confDir   = "config"
	netConf   = confDir + "/net.conf"
	dbFile    = confDir + "/db.conf"
	queueFile = confDir + "/queue.conf"
)

func main() {

	//go func() {
	//    log.Println(http.ListenAndServe("localhost:6060", nil))
	//}()
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case server: // Launch server
			runtime.GOMAXPROCS(2)
			p := new(per.Postgre)
			err := p.InitPostgreFromFile(dbFile, 80)
			if nil != err {
				log.Print(err)
				return
			}
			s := new(queuer.StompManager)
			err = s.ConnectFromFile(queueFile)
			if nil != err {
				log.Print("Couldn't connect to stomp!")
				log.Fatal(err)
			}
			s.Run()
			h := hurler.NewHurler(p, s, 4)
			h.Work(4)
		case initt:
			err := per.CreateConfFile(dbFile)
			if nil != err {
				log.Print("Can't create db conf")
				log.Print(err)
			}
			s := new(queuer.StompManager)
			err = s.CreateConfFile(queueFile)
			if nil != err {
				log.Print("Can't create stomp conf")
				log.Print(err)
			}
		case dbInit:
			p := new(per.Postgre)
			err := p.InitPostgreFromFile(dbFile, 80)
			if nil != err {
				log.Print("Couldn't Connect db!", err)
				return
			}
			p.InitDB()
		default:
			fmt.Println(usage)
		}
	} else {
		fmt.Println(usage)
	}
}
