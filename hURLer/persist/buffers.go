package persist

import (
	"sync"
	"time"
)

// Seen reflects the last time we have seen the element with Id
type Seen struct {
	ID    uint64
	Tseen time.Time
}

// SeenBuff is a simple buffer of Seen Elements
type SeenBuff struct {
	cap   int
	count int
	seen  []Seen
	mutex *sync.Mutex
}

// NewSeenBuff creates a new buffer with the specified capacity
func NewSeenBuff(cap int) *SeenBuff {
	return &SeenBuff{cap, 0, make([]Seen, cap), &sync.Mutex{}}
}

// Lock the internal mutex
func (s *SeenBuff) Lock() {
	s.mutex.Lock()
}

// Unlock the internal mutex
func (s *SeenBuff) Unlock() {
	s.mutex.Unlock()
}

// AddNow Adds an instance seen 'now' to the Buffer
func (s *SeenBuff) AddNow(id uint64) []Seen {
	return s.AddSeen(id, time.Now())
}

// AddSeen Adds an instance to the Buffer
// If the current buffer is full return it and create a new one else return empty
func (s *SeenBuff) AddSeen(id uint64, t time.Time) []Seen {
	var dump []Seen
	s.seen[s.count] = Seen{id, t}
	if s.count >= s.cap {
		dump = make([]Seen, s.cap)
		copy(dump, s.seen)
	}
	s.count = (s.count + 1) % s.cap
	return dump
}
