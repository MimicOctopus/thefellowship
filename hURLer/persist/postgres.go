package persist

import (
	"database/sql"
	h "gitlab.com/MimicOctopus/thefellowship/types"
	pu "github.com/MimicOctopus/goutils/postgre"
	"github.com/hashicorp/golang-lru"
	_ "github.com/lib/pq" //pq is a SQL dependency for this implementation
	"log"
	"time"
)

// Postgre is a structure to help manage the Postgre connections
type Postgre struct {
	db          *sql.DB
	urlSeen     *SeenBuff
	robot       *h.RobotGetter
	domainCache *lru.Cache
	urlCache    *lru.Cache
}

// CreateConfFile creates a Configuration file at the specified location
// Utility function to hide the backing library
func CreateConfFile(file string) error {
	return pu.CreateConfFile(file)
}

// InitPostgreFromFile Inits a Postgre based hURLer back-end from the given config file
func (p *Postgre) InitPostgreFromFile(file string, bufQte int) error {
	db, err := pu.ConnectDBFromFile(file)
	if nil != err {
		return err
	}
	p.db = db
	p.urlSeen = NewSeenBuff(bufQte)
	p.robot = h.NewRobotGetter()
	c, err := lru.New(2000)
	p.domainCache = c
	if nil != err {
		return err
	}
	d, err := lru.New(2000)
	p.urlCache = d
	return err
}

// GetRessourceID obtains the ID for a given ressource, if this ressource exists
func (p *Postgre) GetRessourceID(domainID uint64, proto string, ressource string) (uint64, error) {
	row := p.db.QueryRow("SELECT id FROM url WHERE did=$1 proto=$2 ressource=$3;", domainID, proto, ressource)
	var id uint64
	err := row.Scan(&id)
	return id, err
}

// GetOrInsertDomain Obtains the Domain descriptor from the database or creates it if it didn't already exist
func (p *Postgre) GetOrInsertDomain(s string) (h.Domain, error) {
	row := p.db.QueryRow("SELECT * FROM get_Domain($1)", s)

	var id uint64
	var t time.Time
	var ex bool
	var ban bool
	var statusCode int
	var robot string
	err := row.Scan(&id, &t, &ex, &ban, &statusCode, &robot)
	if err != nil {
		return nil, err
	}
	if "" == robot {
		statusCode, robot, err = p.robot.GetRobot(s)
		if nil != err {
			log.Print(err)
		}
		_, err = p.db.Exec("INSERT INTO robots (id, status, txt, addt) VALUES ($1, $2, $3, $4)", id, statusCode, robot, time.Now())
		if nil != err {
			log.Print("Error inserting robot: ", err)
		}
	}
	return h.NewDDomain(id, s, ex, ban, t, statusCode, robot), err
}

func (p *Postgre) getDomain(domain string) (h.Domain, error) {

	val, ok := p.domainCache.Get(domain)
	if ok {
		return val.(h.Domain), nil
	}

	var ban bool
	var id uint64
	var t time.Time
	var ex bool
	var statusCode int
	var robot string

	row := p.db.QueryRow("SELECT * FROM get_domain($1)", domain)
	err := row.Scan(&id, &t, &ex, &ban, &statusCode, &robot)
	if nil != err {
		return nil, err
	}
	if statusCode == 0 {
		statusCode, robot, err = p.robot.GetRobot("http://" + domain) // FIXME!
		if nil != err {
			log.Print(err)
		}
		_, err = p.db.Exec("INSERT INTO robots (id, status, txt, addt) VALUES ($1, $2, $3, $4)", id, statusCode, robot, time.Now())
		if nil != err {
			return nil, err
		}
	}
	d := h.NewDDomain(id, domain, ex, ban, t, statusCode, robot)
	p.domainCache.Add(domain, d)
	return d, nil
}

// ManageIncomingDomainRessources checks a set or ressources against the DB.
// It returns the structure where appropriate or none if it already existed or was part of a banned domain
func (p *Postgre) ManageIncomingDomainRessources(domRes h.DomainResources) (h.DomainResources, error) {
	d := domRes.Domain()
	if ignoreTLD(d) {
		return nil, nil
	}
	domain, err := p.getDomain(d)
	if nil != err {
		log.Print(err)
		return nil, err
	}
	if domain.Banned() {
		return nil, err
	}
	sub := make(map[string][]string)
	for proto, resources := range domRes.ProtoRes() {
		out := make([]string, len(resources))
		count := 0
		for _, resource := range resources {
			if ignoreExt(resource) {
				continue
			}
			ok, err := domain.Test(resource, "*")
			if nil != err {
				return nil, err
			}
			if !ok { // short circuit
				continue
			}

			var url h.URL
			val, ok := p.urlCache.Get(resource)
			if ok {
				url = val.(h.URL)
			} else {
				var id uint64
				var t time.Time
				var ex bool
				row := p.db.QueryRow("SELECT * FROM get_resource($1, $2, $3)", proto, domain.GetID(), resource)
				err = row.Scan(&id, &t, &ex)
				if nil != err {
					log.Print("Error while querying url: ", err)
					continue
				}
				url = h.NewUUrl(id, resource, ex, t)
				p.urlCache.Add(resource, url)
			}

			err = p.URLSeen(url.GetID())
			if nil != err {
				log.Print("Error marking URLSeen: ", err)
			}

			if url.Exists() {
				continue
			}
			out[count] = resource
			count++
		}
		if count != 0 {
			sub[proto] = out
		}
	}
	ret := h.NewDomainResources(domRes.Domain(), sub)
	return ret, nil
}

// PersistDomain adds a domain to the database
func (p *Postgre) PersistDomain(d h.Domain) error {
	tx, err := p.db.Begin()
	if nil != err {
		return err
	}
	_, err = tx.Exec("INSERT INTO Domain (domain, addt) VALUES ($1,$2)", d.GetDomain(), d.FirstSeen())
	if nil != err {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	d.SetExists(true)
	return err
}

// BanKeyword adds a keyword to the banned keyword list
// Feature is planned but unimplemented yet
func (p *Postgre) BanKeyword(k string) error {
	return nil
}

// BanDomain inserts the given domain to the list of banned domain given it's ID
func (p *Postgre) BanDomain(id uint64) error {
	tx, err := p.db.Begin()
	if nil != err {
		return err
	}
	_, err = tx.Exec("INSERT INTO bandomain (did, t) VALUES ($1, $2);", id, time.Now())
	if nil != err {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	return err
}

// URLSeen registered the time we have seen a URL with the given ID
func (p *Postgre) URLSeen(id uint64) error {
	p.urlSeen.Lock()
	dump := p.urlSeen.AddNow(id)
	p.urlSeen.Unlock()
	if len(dump) == 0 {
		return nil
	}
	tx, err := p.db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("INSERT INTO URLSeen (uid, t) VALUES ($1, $2)")
	if nil != err {
		tx.Rollback()
		return err
	}
	for _, s := range dump {
		_, err = stmt.Exec(s.ID, s.Tseen)
		if nil != err {
			tx.Rollback()
			return err
		}
	}
	err = tx.Commit()
	return err
}

// MarkRessourceFail registers a failure for the Ressource with the given ID
func (p *Postgre) MarkRessourceFail(u uint64) error {
	_, err := p.db.Exec("INSERT INTO failurl (uid, t) VALUES ($1, $2);", u, time.Now())
	return err
}
