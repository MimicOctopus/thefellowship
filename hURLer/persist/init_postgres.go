package persist

import (
	"log"
)

// InitDB enables easy creation of the current SQL schema
// While the schema is currently hard coded in this function, it should eventually be described in it's own file
func (p *Postgre) InitDB() {
	//Drop previous content
	_, err := p.db.Exec(`DROP TABLE IF EXISTS domain,url,failurl,dumap,banlist,bandomain,urlseen,domainseen,robots CASCADE`)
	if nil != err {
		log.Print(err)
	}
	log.Print("Content dropped")

	// Create Domain table
	log.Print("Creating Domain table")
	_, err = p.db.Exec(`CREATE TABLE Domain(
                   id BIGSERIAL PRIMARY KEY,
	               domain VARCHAR(255) NOT NULL UNIQUE,
	               addt TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	log.Print("Creating robots table")
	_, err = p.db.Exec(`CREATE TABLE robots(
                    id BIGINT PRIMARY KEY REFERENCES domain ON DELETE CASCADE,
                    status INTEGER NOT NULL,
                    txt TEXT NOT NULL,
                    addt TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	// Create URL table
	log.Print("Creating URL Table")
	_, err = p.db.Exec(`CREATE TABLE URL(
                   id BIGSERIAL PRIMARY KEY,
                   did BIGINT REFERENCES domain ON DELETE CASCADE,
				   proto text NOT NULL,
	               ressource text NOT NULL,
	               addt TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	log.Print("Creating Failed URL Table")
	_, err = p.db.Exec(`CREATE TABLE failurl(
	               uid BIGINT PRIMARY KEY REFERENCES url ON DELETE CASCADE,
	               t TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	log.Print("Creating URL seen")
	_, err = p.db.Exec(`CREATE TABLE URLSeen(
                  id BIGSERIAL PRIMARY KEY,
	              uid BIGINT NOT NULL REFERENCES URL ON DELETE CASCADE,
	              t TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	// Keyword banning
	log.Print("Banning keywords")
	_, err = p.db.Exec(`CREATE TABLE BanList(
	               id SERIAL NOT NULL PRIMARY KEY,
	               ban text NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	// Domain banning
	log.Print("Banning Domains")
	_, err = p.db.Exec(`CREATE TABLE BanDomain(
	               did BIGINT PRIMARY KEY  REFERENCES Domain ON DELETE CASCADE,
	               t TIMESTAMP NOT NULL)`)
	if nil != err {
		log.Print(err)
	}

	// Create Domain, URL add functions
	log.Print("Creating domain and URL adders")
	_, err = p.db.Exec(`CREATE OR REPLACE FUNCTION
                        get_domain(VARCHAR(255),
                            OUT ddid BIGINT,
                            OUT t TIMESTAMP,
                            OUT exist BOOLEAN,
                            OUT banned BOOLEAN,
                            OUT status INTEGER,
                            OUT txt text) RETURNS RECORD AS $$
                                BEGIN
                                SELECT INTO ddid, t, exist, banned, status, txt
                                    d.id, d.addt, TRUE, (CASE WHEN b.did IS NULL THEN FALSE ELSE TRUE END), r.status, r.txt
                                    FROM Domain d
                                    LEFT JOIN bandomain b ON d.id=b.did
                                    LEFT JOIN robots r ON d.id=r.id
                                    WHERE d.domain=$1;
                                IF ddid IS NULL THEN
                                    t := now();
                                    exist := false;
                                    banned := false;
                                    INSERT INTO Domain (domain, addt) VALUES ($1, t) RETURNING id INTO ddid;
                                END IF;
                                if status IS NULL THEN
                                    status := 0;
                                    txt := '';
                                END IF;
                                END;
	               $$ LANGUAGE plpgsql;`)
	if nil != err {
		log.Print(err)
	}

	_, err = p.db.Exec(`CREATE OR REPLACE FUNCTION
                       get_resource(VARCHAR(10), BIGINT, VARCHAR(2048),
                                         OUT uid BIGINT,
                                         OUT t TIMESTAMP,
                                         OUT exist BOOLEAN) RETURNS RECORD AS $$
                           BEGIN
                               SELECT INTO uid, t, exist
                                id, addt, TRUE
                                FROM URL WHERE did=$2 AND proto=$1 AND URL.ressource=$3;
                               IF uid IS NULL THEN
                               t := now();
                               exist := FALSE;
                               INSERT INTO URL (proto, did, ressource, addt) VALUES ($1, $2, $3, t) RETURNING id INTO uid;
                               END IF;
                           END;
	               $$ LANGUAGE plpgsql;`)
	if nil != err {
		log.Print(err)
	}

}
