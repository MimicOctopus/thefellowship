package persist

import (
	h "gitlab.com/MimicOctopus/thefellowship/types"
)

// HURLerPersistor is an interface describing what a given persistence backend should look like
type HURLerPersistor interface {
	ManageIncomingDomainRessources(domRes h.DomainResources) (h.DomainResources, error)
	GetRessourceID(domainID uint64, proto string, ressource string) (uint64, error)
	GetOrInsertDomain(string) (h.Domain, error)
	PersistDomain(d h.Domain) error
	BanKeyword(keyword string) error
	BanDomain(id uint64) error
	URLSeen(id uint64) error
	MarkRessourceFail(id uint64) error
}
