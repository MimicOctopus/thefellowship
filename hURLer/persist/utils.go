package persist

import (
	"strings"
)

// Ignores the following extensions
func ignoreExt(url string) bool { // For now
	BAD := []string{".zip", ".7z", ".tar", ".iso", ".rar", ".bz2", ".gz", ".exe", ".mp3", ".md5", ".js", ".css", ".xml"}
	for _, bad := range BAD {
		if strings.Contains(url, bad) {
			return true
		}
	}
	return false
}

// Ignores the following TLDs... Temporary
// The list is temporary and based on the fact I can't read foreign langages :(
// ... or that I just have no interest, and limited to those I think are fairly common
func ignoreTLD(dom string) bool {
	BAD := []string{".ru", ".jp", ".cn", ".kr", ".xxx"}
	for _, bad := range BAD {
		if strings.Contains(dom, bad) {
			return true
		}
	}
	return false
}
