package queuer

import "gitlab.com/MimicOctopus/thefellowship/types"

// The QueueManager describes how a wire protocol should be abstracted within hURLer
type QueueManager interface {
	GetSend() chan<- []types.DomainResources
	Receive() <-chan []types.DomainResources
	Bans() <-chan []string
	Fail() <-chan []types.DomainResources
}
