package queuer

import (
	"encoding/json"
	m "gitlab.com/MimicOctopus/thefellowship/messages"
	"gitlab.com/MimicOctopus/thefellowship/types"
	c "github.com/MimicOctopus/goutils/config"
	stomp "github.com/guotie/stomp"
	"log"
	"os"
	"time"
)

// StompManager is a structure letting hURLer interact with Enterprise messaging systems over the stomp protocol
type StompManager struct {
	conf     stompManagerConf
	conn     *stomp.Conn
	outConn  *stomp.Conn
	inSub    *stomp.Subscription
	banSub   *stomp.Subscription
	failSub  *stomp.Subscription
	outQueue string
	in       chan []types.DomainResources
	Out      chan []types.DomainResources
	bans     chan []string
	fail     chan []types.DomainResources
}

// stompManagerConf is the configuration for the StompManager
type stompManagerConf struct {
	Proto     string
	Addr      string
	Opts      stomp.Options
	OutQueue  string
	InQueue   string
	BanQueue  string
	FailQueue string
}

// ConnectStomp Connects to the stomp server with the given options
func (s *StompManager) ConnectStomp(proto string, addr string, opts stomp.Options) error {
	conn, err := stomp.Dial("tcp", addr, opts)
	if nil != err {
		return err
	}
	s.conn = conn
	return nil
}

// ConnectFromFile connects to a Stomp Server using the config from the given file
func (s *StompManager) ConnectFromFile(file string) error {
	fh, err := os.Open(file)
	if nil != err {
		return err
	}

	dec := json.NewDecoder(fh)
	var conf stompManagerConf
	err = dec.Decode(&conf)
	if nil != err {
		return err
	}

	s.conf = conf
	err = s.ConnectRec()
	if nil != err {
		return err
	}
	err = s.ConnectSend()
	return err
}

// ConnectRec prepares the receivers so that they operate in a non-blocking manner
func (s *StompManager) ConnectRec() error {
	err := s.ConnectStomp(s.conf.Proto, s.conf.Addr, s.conf.Opts)
	if nil != err {
		return err
	}
	err = s.SubscribeIncoming(s.conf.InQueue)
	if nil != err {
		return err
	}
	err = s.SubscribeBans(s.conf.BanQueue)
	if nil != err {
		return err
	}
	err = s.SubscribeFail(s.conf.FailQueue)
	if nil != err {
		return err
	}
	return err
}

// ConnectSend prepares the senders so that they operate in a non-blocking manner
func (s *StompManager) ConnectSend() error {
	outConn, err := stomp.Dial("tcp", s.conf.Addr, s.conf.Opts)
	if nil != err {
		return err
	}
	s.outConn = outConn
	s.SetOutQueue(s.conf.OutQueue)
	return nil
}

// CreateConfFile creates a file with the specified name, containing a stomp config
func (s *StompManager) CreateConfFile(file string) error {
	dbconf := stompManagerConf{"tcp", "localhost", stomp.Options{}, "out", "in", "ban", "fail"}

	err := c.WriteConfFile(&dbconf, file)

	return err
}

// SubscribeIncoming Subscribes to an incoming queue with the given name
func (s *StompManager) SubscribeIncoming(queueName string) error {
	var err error
	s.inSub, err = s.conn.Subscribe(queueName, stomp.AckAuto)
	if nil == s.in {
		s.in = make(chan []types.DomainResources, 10)
	}
	return err
}

// SubscribeBans Subscribes to a ban queue with the given name
func (s *StompManager) SubscribeBans(queueName string) error {
	var err error
	s.banSub, err = s.conn.Subscribe(queueName, stomp.AckAuto)
	if nil == s.bans {
		s.bans = make(chan []string, 10)
	}
	return err
}

// SubscribeFail Subscribes to a fail queue with the given name
func (s *StompManager) SubscribeFail(queueName string) error {
	var err error
	s.failSub, err = s.conn.Subscribe(queueName, stomp.AckAuto)
	if nil == s.fail {
		s.fail = make(chan []types.DomainResources, 10)
	}
	return err
}

// SetOutQueue sets the output queue of the stomp manager
func (s *StompManager) SetOutQueue(outQueue string) {
	s.outQueue = outQueue
	if s.Out == nil {
		s.Out = make(chan []types.DomainResources, 10)
	}
}

// GetSend returns the sender channel
func (s *StompManager) GetSend() chan<- []types.DomainResources {
	return s.Out
}

// Receive returns the receiver channel
func (s *StompManager) Receive() <-chan []types.DomainResources {
	return s.in
}

// Bans returns the incoming bans channel
func (s *StompManager) Bans() <-chan []string {
	return s.bans
}

// Fail returns the channel of incoming fails
func (s *StompManager) Fail() <-chan []types.DomainResources {
	return s.fail
}

func (s *StompManager) sender() {
	ticker := time.NewTicker(time.Millisecond * 2000)
	for {
		select { // Give some priority, so we don't lock
		case out := <-s.Out:
			log.Print("Sending Message...")
			//data, err := m.CompressURLs(out)
			data, err := m.CompressDomainURL(out)
			if nil != err {
				log.Print("Can't marshal: ", err)
			}
			err = s.outConn.Send(s.outQueue, "", data, nil)
			if nil != err {
				log.Print(err)
				s.ConnectSend()
				s.Out <- out
				continue
			}
			log.Print("Message sent")
		case <-ticker.C: //Timeout, carry on
			continue
		}
	}
}

func (s *StompManager) receiver() {
	for {
		select {
		case msg := <-s.inSub.C:
			if nil != msg.Err {
				log.Print(msg.Err)
				s.ConnectRec()
				continue
			}
			domains, err := m.UncompressDomainURL(msg.Body)
			//urls, err := m.UncompressURLs(msg.Body)
			if nil != err {
				log.Print("Invalid bundle!: ", err)
				continue
			}
			log.Print("s.in: ", len(s.in))
			//log.Print(domains)
			//s.in <- urls
			s.in <- domains
		case msg := <-s.banSub.C:
			log.Print("Dequeued Ban")
			if nil != msg.Err {
				log.Print(msg.Err)
				s.ConnectRec()
				continue
			}
			domains, err := m.UncompressURLs(msg.Body)
			if nil != err {
				log.Print("---")
				log.Print("Invalid bundle!: ", err)
				log.Print(msg.Body)
				log.Print("---")
				continue
			}
			//s.bans <- urls
			s.bans <- domains
		case msg := <-s.failSub.C:
			if nil != msg.Err {
				log.Print(msg.Err)
				s.ConnectRec()
				continue
			}
			//urls, err := m.UncompressURLs(msg.Body)
			domains, err := m.UncompressDomainURL(msg.Body)
			if nil != err {
				log.Print("---")
				log.Print("Invalid bundle!: ", err)
				log.Print(msg.Body)
				log.Print("---")
				continue
			}
			//s.fail <- urls
			s.fail <- domains
		}
	}
}

// Run executes the receiving and sending tasks of the StompManager asynchronously
func (s *StompManager) Run() {
	go s.sender()
	go s.receiver()
}
