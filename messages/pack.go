package messages

import (
	t "gitlab.com/MimicOctopus/thefellowship/types"
)

// PackDomains packs an Array of DomainResources
func PackDomains(pack interface{}) ([]byte, error) {
	urls := pack.([]t.DomainResources)
	return CompressDomainURL(urls)
}

// UnpackDomains unpacks raw bytes from the network
func UnpackDomains(raw []byte) (interface{}, error) {
	return UncompressDomainURL(raw)
}

// PackBans packs an array of banned domains
func PackBans(pack interface{}) ([]byte, error) {
	bans := pack.([]string)
	return CompressURLs(bans)
}

// UnpackBans unpacks an array of banned domains
func UnpackBans(raw []byte) (interface{}, error) {
	return UncompressURLs(raw)
}

// PackHTML packs an HTMLResource
func PackHTML(pack interface{}) ([]byte, error) {
	html := pack.(t.HTMLResource)
	return CompressHTML(html)
}

// UnpackHTML unpacks an HTMLResource
func UnpackHTML(raw []byte) (interface{}, error) {
	return UncompressHTML(raw)
}
