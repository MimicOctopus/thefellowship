//go:generate protoc --go_out=. messages.proto

// Package messages provides structures and utilities for the wire protocols
package messages

import (
	"bytes"
	"compress/zlib"
	t "gitlab.com/MimicOctopus/thefellowship/types"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
)

// bundleDomainURL bundles the array of DomainResources into a DomainPack
func bundleDomainURL(urls []t.DomainResources) *DomainPack {
	pack := make([]*DomainUrls, len(urls))
	i := 0
	for _, v := range urls {
		domain := v.Domain()
		pack[i] = &DomainUrls{Domain: &domain, Http: v.Resources("http"), Https: v.Resources("https")}
		i++
	}
	domainPack := DomainPack{Domains: pack}

	return &domainPack
}

// GetBundleDomainWire bundles the map[domain][]ressources into a DomainPack and then marshals them
func GetBundleDomainWire(urls []t.DomainResources) ([]byte, error) {
	protomsg := bundleDomainURL(urls)
	data, err := proto.Marshal(protomsg)
	return data, err
}

// CompressDomainURL Compresses the map[domain][]ressources after bundling it into a DomainPack to save bandwidth
func CompressDomainURL(urls []t.DomainResources) ([]byte, error) {
	data, err := GetBundleDomainWire(urls)
	if nil != err {
		return nil, err
	}
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return b.Bytes(), nil
}

// UncompressDomainURL gets back a []DomainResources from a compressed pack
func UncompressDomainURL(data []byte) ([]t.DomainResources, error) {
	r, err := zlib.NewReader(bytes.NewReader(data))
	if nil != err {
		return nil, err
	}
	b, err := ioutil.ReadAll(r)
	if nil != err {
		return nil, err
	}
	bundle := &DomainPack{}
	err = proto.Unmarshal(b, bundle)
	d := bundle.GetDomains()
	out := make([]t.DomainResources, len(d))
	i := 0
	for _, v := range d {
		out[i] = t.NewDomainResources(v.GetDomain(), map[string][]string{"http": v.GetHttp(), "https": v.GetHttps()})
		i++
	}
	return out, nil
}
