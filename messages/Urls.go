// Package messages provides structures and utilities for the wire protocols
package messages

import (
	"bytes"
	"compress/zlib"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
)

// bundleURLs bundles a bunch of complete URL strings into the proper protobuf message
func bundleURLs(urls []string) *UrlBundle {
	return &UrlBundle{Url: urls}
}

// GetBundleWire bundles a complete URL strings into the proper protobuf message AND Marshal's it
func GetBundleWire(urls []string) ([]byte, error) {
	protomsg := bundleURLs(urls)
	data, err := proto.Marshal(protomsg)
	return data, err
}

// CompressURLs compresses the given URL strings after Bundling them as Protobuf object to save bandwidth
func CompressURLs(urls []string) ([]byte, error) {
	data, err := GetBundleWire(urls)
	if nil != err {
		return nil, err
	}
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return b.Bytes(), nil
}

// UncompressURLs gets URLs strings from a compressed Protobuf object
func UncompressURLs(data []byte) ([]string, error) {
	r, err := zlib.NewReader(bytes.NewReader(data))
	if nil != err {
		return nil, err
	}
	b, err := ioutil.ReadAll(r)
	if nil != err {
		return nil, err
	}
	bundle := &UrlBundle{}
	err = proto.Unmarshal(b, bundle)
	return bundle.GetUrl(), err
}
