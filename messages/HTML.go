package messages

import (
	"bytes"
	"compress/zlib"
	t "gitlab.com/MimicOctopus/thefellowship/types"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
)

// packHTML packs string representing the content of an HTML page
func packHTML(html t.HTMLResource) *HtmlContent {
	return &HtmlContent{Html: proto.String(html.HTML()), Src: proto.String(html.SourceURLString())}
}

// GetHTMLWire takes the html contents and marshals it into a wire ready protobuf object
func GetHTMLWire(html t.HTMLResource) ([]byte, error) {
	protomsg := packHTML(html)
	data, err := proto.Marshal(protomsg)
	return data, err
}

// CompressHTML takes html content and compresses it after marshalling, to save bandwidth
func CompressHTML(html t.HTMLResource) ([]byte, error) {
	data, err := GetHTMLWire(html)
	if nil != err {
		return nil, err
	}
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return b.Bytes(), nil
}

// UncompressHTML takes the network received compressed html object and extracts it
func UncompressHTML(data []byte) (*t.HTMLResource, error) {
	r, err := zlib.NewReader(bytes.NewReader(data))
	if nil != err {
		return nil, err
	}
	b, err := ioutil.ReadAll(r)
	if nil != err {
		return nil, err
	}
	html := &HtmlContent{}
	err = proto.Unmarshal(b, html)
	ret, err := t.NewHTMLResource(html.GetSrc(), html.GetHtml())
	return &ret, err
}
