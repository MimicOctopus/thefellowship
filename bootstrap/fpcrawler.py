#!/usr/bin/env python
# -*- coding: utf-8 -*-
import httplib2
from BeautifulSoup import BeautifulSoup, SoupStrainer
import sets


FRONTPAGES = "frontpages.lst"

def getUrls():
    http = httplib2.Http()
    urls = set()
    with open(FRONTPAGES, 'r') as fp:
        for i in fp:
            i = i.strip()
            if i == "":
                continue
            print("Crawling: " + i)
            try:
                status, response = http.request(i)
            except:
                continue
            soup = BeautifulSoup(response)
            for link in soup.findAll('a', href=True):
                if None is link:
                    continue
                a = link['href']
                if len(a) < 7: 
                    continue
                if len(a) > 2083:
                    print("what the hell man")
                    continue
                if "http" in a[0:4]:
                    urls.add(a)
    return urls

if __name__ == '__main__':
    urls = getUrls()
    with open('urls.txt', 'wb') as f:
        for url in urls:
            f.write(("%s\n" % url).encode("UTF8"))
    
