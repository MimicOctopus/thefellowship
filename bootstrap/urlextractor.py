#!/usr/bin/env python
# -*- coding: utf-8 -*-
from BeautifulSoup import BeautifulSoup, SoupStrainer
from stompest.config import StompConfig
from stompest.sync import Stomp
from stompest.protocol import StompSpec
import sets
import random
import wirewrap

# Everything relative to hURLer config
HOST = "myhost"
CONFIG = StompConfig('tcp://myhost:port', login="user", passcode="pass", version=StompSpec.VERSION_1_2)
INQUEUE = 'something'
OUTQUEUE = 'something'

ID = random.randint(0,99999999)

def getAllUrls(html):
    urls = set()
    soup = BeautifulSoup(html)
    for link in soup.findAll('a', href=True):
        if None is link:
            continue
        a = link['href']
        if len(a) < 7:
            continue
        if len(a) > 2083:
             print("This url looks funky :%s" % a)
             continue 
        if "http" in a[0:4]:
             urls.add(a)
    return urls

if __name__=="__main__":
    client = Stomp(CONFIG)
    client.connect(host=HOST)
    client.subscribe(OUTQUEUE, {'id': ID, StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})
    while True:
        print("Waiting for frame")
        frame = client.receiveFrame()
        client.ack(frame)
        html = wirewrap.getHTML(frame.body)
        urls = getAllUrls(html)
        print("Done parsing")
        for message in wirewrap.getMessages(urls):
            client.send(INQUEUE, message, headers={'content-type':'application/octet-stream', 'content-length':len(message)})
