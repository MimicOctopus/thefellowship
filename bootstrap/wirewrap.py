#!/usr/bin/env python
# -*- coding: utf-8 -*-
import messages_pb2
import zlib

"""Wraps the original protocol"""

def getHTML(data):
    content = zlib.decompress(data)
    htmlContent = messages_pb2.htmlContent()  
    htmlContent.ParseFromString(content)
    return htmlContent.html

def getMessages(urls):
    i = 0
    message = []
    for u in urls:
        message += [u]
        if i == 199:
            yield readyForWire(message)
            message = []
        i = (i+1)%200
    yield readyForWire(message)

def readyForWire(message):
    bundle = messages_pb2.urlBundle()
    bundle.url.extend(message)
    data = bundle.SerializeToString()
    return zlib.compress(data)
