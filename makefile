
## Some shortcuts

default: build

build:
	go generate ./...
	go build ./...

## Runing make clean will sanitize the directory structure
#
clean:
	$(RM) messages/messages.pb.go hURLer/hURLer crawl/crawl parse/parse load/load


fmt:
	go fmt ./...

lint:
	golint ./...


