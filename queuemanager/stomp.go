package queuemanager

import (
	t "gitlab.com/MimicOctopus/thefellowship/types"
	config "github.com/MimicOctopus/goutils/config"
	stomp "github.com/guotie/stomp"
	"log"
)

// StompManager implements the QueueManager interface for the Stomp Protocol
type StompManager struct {
	conf stompManagerConf
}

type stompManagerConf struct {
	Proto string
	Addr  string
	Opts  stomp.Options
}

type stompChan struct {
	queueName string
	conf      stompManagerConf
	conn      *stomp.Conn
}

// InitFromFile inits the bare StompManager from the provided config file
func (s *StompManager) InitFromFile(file string) (err error) {
	conf := stompManagerConf{}
	err = config.ReadConfFile(&conf, file)
	s.conf = conf
	return err
}

// CreateConfFile creates a new, empty config file with the given location
func (s *StompManager) CreateConfFile(file string) (err error) {
	conf := stompManagerConf{"tcp", "localhost", stomp.Options{}}
	err = config.WriteConfFile(&conf, file)
	return err
}

func newStompChan(conf stompManagerConf, queueName string) (*stompChan, error) {
	s := &stompChan{queueName, conf, nil}
	err := s.connectStomp()
	return s, err
}

func (s *stompChan) connectStomp() error {
	conn, err := stomp.Dial(s.conf.Proto, s.conf.Addr, s.conf.Opts)
	if nil != err {
		return err
	}
	s.conn = conn
	return nil
}

func (s *stompChan) send(data []byte) error {
	return s.conn.Send(s.queueName, "", data, nil)
}

func (s *stompChan) subscribe() (*stomp.Subscription, error) {
	return s.conn.Subscribe(s.queueName, stomp.AckAuto)
}

// Send connects to the server and sends the incoming structures to the stomp server
func (s *StompManager) Send(queue string, pack t.Packer, c <-chan interface{}, done chan<- bool) {
	sChan, err := newStompChan(s.conf, queue)
	if nil != err {
		log.Print("Error creating stomp connection: ", err)
		return // Handle error?
	}
	log.Printf("Channel `%s` ready", queue)
	for out := range c {
		log.Print("Preparing Message")
		data, err := pack(out)
		if nil != err {
			log.Print("Can't marshal: ", err)
			continue
		}
		err = sChan.send(data)
		if nil != err {
			log.Print(err)
			done <- false
			return //Handle error... Error chanel?
		}
		log.Print("Message sent")
	}
	done <- true
}

// Receive connects to the server and listens to the given queue forwarding unpacked messages to the owner of the channel
func (s *StompManager) Receive(queue string, unpack t.UnPacker, c chan<- interface{}, done <-chan bool) {
	sChan, err := newStompChan(s.conf, queue)
	if nil != err {
		log.Print("Error creating stomp connection: ", err)
		return // Handle error?
	}
	in, err := sChan.subscribe()
	if nil != err {
		log.Print("Error subscribing: ", err)
		return // Handle better?
	}
	log.Printf("Channel `%s` ready", queue)
	for {
		select {
		case msg := <-in.C:
			if nil != msg.Err {
				log.Print(msg.Err) // Handle
			}
			elems, err := unpack(msg.Body)
			if nil != err {
				log.Print("Invalid artifact!: ", err)
				continue
			}
			c <- elems
		case _ = <-done:
			in.Unsubscribe()
			close(c)
			return
		}
	}
}
