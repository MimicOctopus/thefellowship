package queuemanager

import (
	t "gitlab.com/MimicOctopus/thefellowship/types"
)

// QueueManager describes how a wire protocol should be abstracted within hURLer
type QueueManager interface {
	Send(queue string, fn t.Packer, c <-chan interface{}, done chan<- bool)
	Receive(queue string, fn t.UnPacker, c chan<- interface{}, done <-chan bool)
}
