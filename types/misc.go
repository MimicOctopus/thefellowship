package types

import (
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

var timeout = time.Duration(10 * time.Second)

// RobotGetter is a struct to help obtain Robot.txt files
type RobotGetter struct {
	client http.Client
}

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

// NewRobotGetter instanciates a usable RobotGetter
func NewRobotGetter() *RobotGetter {
	transport := http.Transport{
		Dial: dialTimeout,
	}

	client := http.Client{
		Transport: &transport,
	}
	return &RobotGetter{client}
}

// GetRobot obtains the Robot info for the given robot.txt URL
func (r *RobotGetter) GetRobot(s string) (int, string, error) {
	res, err := r.client.Get(s + "/robots.txt")
	if err != nil {
		log.Print(err)
		if res != nil && res.Body != nil {
			res.Body.Close()
		}
		return 500, "", err
	}
	defer res.Body.Close()
	//if res.StatusCode == 404 { // Fuck it
	//	return 500, "", nil
	//}
	txt, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Print(err)
		return 500, "", nil
	}
	return res.StatusCode, string(txt), nil
}
