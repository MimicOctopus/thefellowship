package types

// Packer is a function type that takes an argument and makes it wireready
type Packer func(pack interface{}) ([]byte, error)

// UnPacker is a function that takes an array of raw network obtained bytes and unpacks the underlying data
type UnPacker func(raw []byte) (interface{}, error)
