package types

import (
	"net/url"
	"time"
)

// URL Represents a hURLer URL
type URL interface {
	SetURL(s string) // Changing the inner url field also "resets" a URL
	GetURL() string
	GetDomain() (string, error) // Gets the domain contained in this URL
	GetID() uint64
	Exists() bool // Wether this URL has been persisted
	SetExists(b bool)
	FirstSeen() time.Time // The first time we've seen this URL
}

// UURL is the base URL implementation
type UURL struct {
	id        uint64
	url       string
	exists    bool
	firstSeen time.Time
}

// NewUUrl creates a brand new URL with an underlying UURL
func NewUUrl(id uint64, url string, exists bool, firstSeen time.Time) URL {
	return UURL{id, url, exists, firstSeen}
}

// UURLFromStringURL creates a brand new URL with the underlying UURL from the give string
func UURLFromStringURL(s string) URL {
	return UURL{0, s, false, time.Now()}
}

// SetURL sets the URL in the UURL Structure
func (u UURL) SetURL(s string) {
	u.url = s
	u.id = 0
	u.firstSeen = time.Now()
	u.exists = false
}

// GetURL gets that URL String back
func (u UURL) GetURL() string {
	return u.url
}

// GetDomain obtains the domain part of the URL
func (u UURL) GetDomain() (string, error) {
	d, err := url.Parse(u.url)
	if nil != err {
		return "", err
	}
	return d.Host, err
}

// GetID gets the ID of this UURL
func (u UURL) GetID() uint64 {
	return u.id
}

// Exists returns a boolean indicating whether we have seen this URL before
func (u UURL) Exists() bool {
	return u.exists
}

// SetExists sets the existance status of this URL
func (u UURL) SetExists(b bool) {
	u.exists = b
}

// FirstSeen returns the timestamp of the moment we have first seen this URL
func (u UURL) FirstSeen() time.Time {
	return u.firstSeen
}
