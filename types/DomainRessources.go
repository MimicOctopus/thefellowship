package types

import (
	"net/url"
)

// DomainResources is an interface describing a bundle of Ressources grouped by domain
type DomainResources interface {
	Domain() string
	Protos() []string
	ProtoRes() map[string][]string
	Resources(proto string) []string
	Add(proto string, resource string)
}

// DR is a structure that implements DomainResources
type DR struct {
	domain   string
	protoRes map[string][]string
}

// NewDomainResources Creates a new struct of type DomainResources
func NewDomainResources(domain string, protoRes map[string][]string) DomainResources {
	return &DR{domain, protoRes}
}

// Domain returns the domain of this DomainResource
func (d *DR) Domain() string {
	return d.domain
}

// Protos returns a list of protocols contained in this DomainResources
func (d *DR) Protos() []string {
	keys := make([]string, len(d.protoRes))
	i := 0
	for key := range d.protoRes {
		keys[i] = key
		i++
	}
	return keys
}

// ProtoRes returns the map{protocol}[]ressources field
func (d *DR) ProtoRes() map[string][]string {
	return d.protoRes
}

// Resources returns the resources associated with the given protocol, if there are any
func (d *DR) Resources(proto string) []string {
	return d.protoRes[proto]
}

// Add add the specified resource to the specified proto array
func (d *DR) Add(proto string, resource string) {
	d.protoRes[proto] = append(d.protoRes[proto], resource)
}

// UrlsToDomainResourcesArray takes a raw url array and turns it into a DomainResources Pack
func UrlsToDomainResourcesArray(origin []string) []DomainResources {
	temp := make(map[string]DomainResources)
	for _, v := range origin {
		url, err := url.Parse(v)
		if nil != err {
			continue
		}
		if nil != temp[url.Host] {
			temp[url.Host].Add(url.Scheme, url.Path)
		} else {
			r := make([]string, 1)
			m := make(map[string][]string)
			r[0] = url.Path
			m[url.Scheme] = r
			temp[url.Host] = NewDomainResources(url.Host, m)
		}
	}
	urls := make([]DomainResources, len(temp))
	i := 0
	for _, v := range temp {
		urls[i] = v
		i++
	}
	return urls
}
