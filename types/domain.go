package types

import (
	"errors"
	"github.com/temoto/robotstxt-go"
	"time"
)

// Domain is an interface to represent domains
type Domain interface {
	SetDomain(s string) error // Changing the inner Domain also "resets" a domain
	GetDomain() string
	GetID() uint64
	Exists() bool // Wether this Domain has been persisted
	Banned() bool
	SetExists(e bool)
	FirstSeen() time.Time // The first time we've seen this URL
	SetRobot(statusCode int, txt string) error
	Test(url string, userAgent string) (bool, error) // Test if we are allowed to crawl this url
}

// DDomain is a struct able to represent a Domain
type DDomain struct {
	id        uint64
	domain    string
	exists    bool
	banned    bool
	firstSeen time.Time
	robot     *robotstxt.RobotsData
}

// NewDDomain creates a new DDomain from the given info
func NewDDomain(id uint64, domain string, exists bool, banned bool, firstSeen time.Time, statusCode int, txt string) Domain {
	robot, _ := robotstxt.FromStatusAndBytes(statusCode, []byte(txt))
	return &DDomain{id, domain, exists, banned, firstSeen, robot}
}

// DDomainFromStringDomain creates a new Domain from a simple string
func DDomainFromStringDomain(s string) Domain {
	return &DDomain{0, s, false, false, time.Now(), nil}
}

// SetDomain sets the inner domain of the calling struct
func (d *DDomain) SetDomain(s string) error {
	if len(s) > 255 {
		return errors.New("This domain is too long!!!")
	}
	d.domain = s
	d.firstSeen = time.Now()
	d.exists = false
	return nil
}

// GetDomain returns the domain string of this domain
func (d *DDomain) GetDomain() string {
	return d.domain
}

// GetID returns the id of this Domain
func (d *DDomain) GetID() uint64 {
	return d.id
}

// Exists returns a boolean indicating whether this domain has been seen before
func (d *DDomain) Exists() bool {
	return d.exists
}

// SetExists sets the existance status of this Domain
func (d *DDomain) SetExists(b bool) {
	d.exists = b
}

// Banned returns a boolean indicating whether that domain has been banned
func (d *DDomain) Banned() bool {
	return d.banned
}

// FirstSeen returns a the time stamp indicating when we've seen this domain
func (d *DDomain) FirstSeen() time.Time {
	return d.firstSeen
}

// SetRobot sets the Robot.txt info for this domain
func (d *DDomain) SetRobot(statusCode int, txt string) error {
	robot, err := robotstxt.FromStatusAndBytes(statusCode, []byte(txt))
	d.robot = robot
	return err
}

// Test checks whether we can query this domain for the given URL and userAgent
func (d *DDomain) Test(url string, userAgent string) (bool, error) {
	if d.robot == nil {
		return false, errors.New("Robot is nil!")
	}
	return d.robot.TestAgent(url, userAgent), nil
}
