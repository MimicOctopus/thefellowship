package types

import (
	"net/url"
)

// HTMLResource wraps the metadata of an HTML resource
type HTMLResource struct {
	html string
	src  *url.URL
}

// NewHTMLResource initializes an HTMLResource struct from the provided source url && content
func NewHTMLResource(source string, html string) (HTMLResource, error) {
	url, err := url.Parse(source)
	if nil != err {
		return HTMLResource{}, err
	}
	return HTMLResource{html, url}, nil
}

// HTML returns the HTML content of the HTMLResource
func (h HTMLResource) HTML() string {
	return h.html
}

// SourceURLString returns the source URL of the resource as a string
func (h HTMLResource) SourceURLString() string {
	return h.src.String()
}
