package parser

import (
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	config "github.com/MimicOctopus/goutils/config"
)

// Config describes the parser configuration
type Config struct {
	SendQ     string
	ReceiveQ  string
	NbWorker  int
	QueueSize int
}

// NewFromFile inits the bare Parser from the provided config file
func NewFromFile(file string, queueMng q.QueueManager) (p *Parser, err error) {
	conf := Config{}
	err = config.ReadConfFile(&conf, file)
	p = NewParser(conf, queueMng)
	return
}

// CreateConfFile creates a new, empty config file with the given location
func CreateConfFile(file string) (err error) {
	conf := Config{"parserout", "parserin", 4, 10}
	return config.WriteConfFile(&conf, file)
}
