package parser

import (
	"bytes"
	t "gitlab.com/MimicOctopus/thefellowship/types"
	s "github.com/deckarep/golang-set"
	"golang.org/x/net/html"
	"strings"
)

// getHref is Helper function to pull the href attribute from a Token
func getHref(t html.Token) (ok bool, href string) {
	// Iterate over all of the Token's attributes until we find an "href"
	for _, a := range t.Attr {
		if a.Key == "href" {
			href = a.Val
			ok = true
		}
	}
	return
}

// getURLs Gets Valid URLs from some content
func getURLs(content t.HTMLResource) []string {
	set := s.NewThreadUnsafeSet()
	reader := bytes.NewReader([]byte(content.HTML()))
	z := html.NewTokenizer(reader)
	for {
		tt := z.Next()
		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			break
		case tt == html.StartTagToken:
			t := z.Token()
			// Check if the token is an <a> tag
			if !(t.Data == "a") {
				continue
			}
			// Extract the href value, if there is one
			ok, url := getHref(t)
			if !ok {
				continue
			}
			// Make sure the url begins with `http`
			if !(len(url) > 10 && url[0:4] == "http") {
				continue
			}
			if len(url) > 2083 {
				continue
			}
			if strings.Contains(url, "localhost") {
				continue
			}
			set.Add(url)
		}
	}

	temp := make([]string, set.Cardinality())
	i := 0
	c := set.Iter()
	for item := range c {
		temp[i] = item.(string)
		i++
	}
	return temp
}
