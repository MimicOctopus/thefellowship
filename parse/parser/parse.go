package parser

import (
	m "gitlab.com/MimicOctopus/thefellowship/messages"
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	t "gitlab.com/MimicOctopus/thefellowship/types"
	"sync"
)

// Parser is a struct facilitating the handling of the parser's logic
type Parser struct {
	queueMng q.QueueManager
	conf     Config
	sender   chan interface{}
	doneSend chan bool
	receiver chan interface{}
	doneRec  chan bool
}

// NewParser takes care of creating a new valid Parser given the provided QueueManager
func NewParser(conf Config, queueMng q.QueueManager) *Parser {
	return &Parser{queueMng, conf,
		make(chan interface{}, conf.QueueSize), make(chan bool, conf.NbWorker),
		make(chan interface{}, conf.QueueSize), make(chan bool, conf.NbWorker)}
}

// Parse launches the appropriate number of Parser workers
func (p Parser) Parse() {
	// Prime queues
	go p.queueMng.Send(p.conf.SendQ, m.PackDomains, p.sender, p.doneSend)
	go p.queueMng.Receive(p.conf.ReceiveQ, m.UnpackHTML, p.receiver, p.doneRec)

	var wg sync.WaitGroup
	wg.Add(p.conf.NbWorker)
	for i := 0; i < p.conf.NbWorker; i++ {
		go p.receiveAndSend()
	}
	wg.Wait()
}

// receiveAndSend manages the main processing loop of a worker
func (p Parser) receiveAndSend() {
	for {
		content := <-p.receiver
		temp := getURLs(content.(t.HTMLResource))
		urls := t.UrlsToDomainResourcesArray(temp)

		p.sender <- urls
	}
}

// TODO: The rest of this + the queuer stuff
