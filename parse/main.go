package main

import (
	"fmt"
	"gitlab.com/MimicOctopus/thefellowship/parse/parser"
	q "gitlab.com/MimicOctopus/thefellowship/queuemanager"
	"log"
	"os"
	"runtime"
)

const (
	server    = "server"
	initt     = "init"
	usage     = `Usage: parser {init|server}`
	confBase  = "config/"
	queueFile = "queue.conf"
	confFile  = "parser.conf"
)

func main() {
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case server: // Launch server
			runtime.GOMAXPROCS(10)
			s := new(q.StompManager)
			err := s.InitFromFile(queueFile)
			if nil != err {
				log.Print("Couldn't connect to stomp!")
				log.Fatal(err)
			}
			p, err := parser.NewFromFile(confFile, s)
			if nil != err {
				log.Print("Loading Config Failed!")
				log.Fatal(err)
			}
			p.Parse()
		case initt:
			s := new(q.StompManager)
			err := s.CreateConfFile(queueFile)
			if nil != err {
				log.Print("Couldn't create queue config file")
				log.Fatal(err)
			}
			err = parser.CreateConfFile(confFile)
			if nil != err {
				log.Print("Couldn't create parser config file")
				log.Fatal(err)
			}
		default:
			fmt.Println(usage)
		}
	} else {
		fmt.Println(usage)
	}
}
